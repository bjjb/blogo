# Blogo 

## a blog/website builder in Go

Blogo processes [Markdown][] files in a directory, and makes HTML files.

## Installation

You need [Go][] installed [properly][http://golang.org/doc/install]. Then run

    go get http://bitbucket.org/bjjb/blogo

## Usage

Then just run `blogo` in your blogo directory.

## Rationale

I wrote this because I wanted to learn to program in Go, and DIY blog engines
are how one learns a language these days.

## Documentation

...is in `blogo.go`, so you can view it with [godoc][]. For usage
instructions, try `blogo -h` or `blogo help`.

## License

The code is available under the [same license][license] as Go.

## TODO

- Put posts in their own directories (with an index.html), but make this
  behaviour selectable.

[Markdown]: http://daringfireball.net/projects/markdown/
[JSON]: http://json.org/
[Go]: http://golang.org
[blackfriday]: https://github.com/russross/blackfriday
[license]: http://golang.org/LICENSE
[blogo.go]: http://bjjb.bitbucket.org/blogo
[godoc]: http://godoc.org
