// Command-line executable for blogo
//
// Works in the directory given on the command-line, or the current working
// directory. Looks for .json files, and for each one found, builds a Post.
// Post values which are absent from a post's .json file will be filled in
// using values from index.json, the blog's configuration. Finally, index.json
// will be rendered using the blog, and pages for every month creating posts
// will be created, and a page for every tag.
//
// A post's body (and summary) is written in Markdown. By default, this is
// obtained by reading a file with the same slug as the .json config, with a
// .md extension. This is also a mustache template, through for which the post
// is the view.
//
// The template used for posts is _post.html, for tags: _tag.html, and for
// months, _month.html. All posts and pages are sent through _layout.html.
// These are all Mustache templates.
//
// By default, generated HTML files will be placed in the same directory. You
// can override this with the "output" value in the index.json.
package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"runtime"
	"strings"
	"time"

	"github.com/hoisie/mustache"
	"github.com/russross/blackfriday"
)

// Version of the program
var Version = "0.0.2"

// Printed out for help on the command-line
var usage = `
Usage:
  blogo [flags]

  Builds a website

  Flags:
  -d DIR  Set the source directory (default is '.')
  -h      Print this help, and exit
  -v      Be verbose
  -V      Print the version number, and exit

  If the directory isn't a blogo directory, it will be populated with a
  _post.html file, which you can use as a starting point.
  
  You need to have a _post.html (which renders a single post) and a
  _index.html, which renders a list of posts, in the directory (as mustache
  templates).
`

// Run the program
func main() {

	dir := "."
	version, help, verbose := false, false, false

	flag.StringVar(&dir, "d", ".", "set the blogo directory (default: .)")
	flag.BoolVar(&help, "h", false, "print help and exit")
	flag.BoolVar(&version, "V", false, "print the version and exit")
	flag.BoolVar(&verbose, "v", false, "set the output directory")

	flag.Usage = func() { fmt.Println(usage) }

	flag.Parse()

	if help {
		flag.Usage()
		return
	}
	if version {
		fmt.Println("blogo v" + Version)
		return
	}

	if err := os.Chdir(dir); err != nil {
		log.Fatal(err)
	}

	log.Fatal(rebuild())
}

// A structure containing data that the `_post.html` mustache template expects
// to be able to render.
type post struct {
	// The HTML content of the post
	Body string
	// The title of the post
	Title string
	// When the post was written (the file's creation time)
	Time string
	// Human readable date representing the Time
	Date string
}

// Rebuilds the blog. Will initialize the directory if needed. Then it will
// build each markdown file it finds in the directory (i.e., those with a .md
// suffix).
func rebuild() error {
	if err := setup(); err != nil {
		return err
	}

	var posts []*post
	if files, err := filepath.Glob("*.md"); err == nil {
		for _, file := range files {
			if post, err := build(file); err != nil {
				posts = append(posts, post)
			}
		}
	} else {
		return err
	}
	return nil
}

// Initialises the current directory, by creating a _post.html.
// If the file exists, nothing happens. Otherwise an error may be returned if
// os.Stat("_post.html") can't be read.
func setup() error {
	if _, err := os.Stat("_post.html"); err == nil {
		return nil
	}

	content := `<!doctype html>
<html>
  <head>
      <meta charset="utf8">
      <title>{{Title}} ∴ My Blog</title>
    <style>
      body { font-size; 1.5em; padding: 2em; }
      h1 { line-height: 2em; border-bottom: 1px solid black; }
    </style>
    <script>
      console.log("Hello, world!");
    </script>
  </head>
  <body>
    <article>
      <header>
        <h1>{{Title}}</h1>
        <time datetime="{{Time}}">{{Date}}</time>
      </header>
      {{{Body}}}
    </article>
  </body>
</html>`

	if err := ioutil.WriteFile("_post.html", []byte(content), 0644); err != nil {
		return err
	}
	content = `
My Blog
=======

- [My First Post](/my-first-post.html)
`
	if err := ioutil.WriteFile("index.md", []byte(content), 0644); err != nil {
		return err
	}
	content = `
My First Post
=============

Hello, world!
`
	if err := ioutil.WriteFile("my-first-post.md", []byte(content), 0644); err != nil {
		return err
	}
	return nil
}

// Builds a single post from the source in the file `file`. This should be
// Markdown. The result is passed into the mustache template in `_post.html`,
// and the output is written to a file with the same name, but a `.html`
// extension.
func build(file string) (*post, error) {
	p := new(post)
	data, err := ioutil.ReadFile(file)
	if err != nil {
		log.Fatal(err)
	}
	p.Body = string(blackfriday.MarkdownCommon(data))
	data, err = ioutil.ReadFile("_post.html")
	if err != nil {
		log.Fatal(err)
	}
	base := file[:len(file)-len(filepath.Ext(file))]
	p.Title = titleize(base)
	info, _ := os.Stat(file)
	p.Time = info.ModTime().UTC().Format(time.RFC3339)
	p.Date = info.ModTime().Format("2 Jan 2006")
	page := base + ".html"
	html := mustache.Render(string(data), p)
	err = ioutil.WriteFile(page, []byte(html), 0644)
	if err != nil {
		log.Fatal(err)
	}
	return p, nil
}

// Utility function to turn "my-blog-post" into "My Blog Post".
func titleize(s string) string {
	parts := strings.Split(s, "-")
	for i, part := range parts {
		parts[i] = strings.Title(strings.Trim(part, " "))
	}
	return strings.Join(parts, " ")
}

func templateDir() string {
	_, f, _, _ := runtime.Caller(0)
	return filepath.Join(filepath.Dir(f), "templates")
}

var defaultTemplates = map[string]string{
	".blogo/post.html":          ``,
	".blogo/post.xml":           ``,
	".blogo/index.html":         ``,
	".blogo/index.xml":          ``,
	".blogo/config.json":        ``,
	".blogo/post.css":           ``,
	"example/index.md":          ``,
	"example/config.json":       ``,
	"example/manifest.appcache": ``,
	".htaccess":                 ``,
	"style.css":                 ``,
}

// vi:ts=4:sw=4
