package main

import (
	"io/ioutil"
	"os"
	"path/filepath"
	"runtime"
	"testing"
	"time"
)

func TestBlogo(t *testing.T) {
	tmpdir, _ := ioutil.TempDir("", "blogo_test")

	_ = os.Chdir(tmpdir)

	time, _ := time.Parse("2006-01-02 UTC", "2014-01-01 UTC")
	_ = ioutil.WriteFile("test.md", []byte("*A* _b_"), 0777)
	_ = os.Chtimes("test.md", time, time)

	if err := rebuild(); err != nil {
		t.Errorf("failed to rebuild: %e", err)
	}

	data, err := ioutil.ReadFile("test.html")

	if err != nil {
		t.Errorf("test.html not readable: %e", err)
	}

	content := string(data)

	expected := `<!doctype html>
<html>
  <head>
      <meta charset="utf8">
      <title>Test ∴ My Blog</title>
    <style>
      body { font-size; 1.5em; padding: 2em; }
      h1 { line-height: 2em; border-bottom: 1px solid black; }
    </style>
    <script>
      console.log("Hello, world!");
    </script>
  </head>
  <body>
    <article>
      <header>
        <h1>Test</h1>
        <time datetime="2014-01-01T00:00:00Z">1 Jan 2014</time>
      </header>
      <p><em>A</em> <em>b</em></p>

    </article>
  </body>
</html>`
	assertEqual(expected, content, t)
}

func BenchmarkBlogo(b *testing.B) {
}

func TestTemplateDir(t *testing.T) {
	_, f, _, _ := runtime.Caller(0)
	expected := filepath.Join(filepath.Dir(f), "templates")
	actual := templateDir()
	assertEqual(expected, actual, t)
}

func assertEqual(s1, s2 string, t *testing.T) {
	if s1 != s2 {
		t.Errorf("`%s` != `%s`", s1, s2)
	}
}
